# Soziale Konflikte

## Grundbegriffe: sozialer Konflikt

```mermaid
graph TD
    A[Konflikte] --> A01[intrapersonelle Konflikte]
    A01 --> A1[Entscheidungskonflikt]
    A1 --> B1[Appetenzkonflikt]
    B1 --> B111[Wahl zwischen <br> 2 ersehenswerte <br> aber gegenseitig <br> ausschließende Ziele] 
    A1 --> C1[Aversionskonflikt]
    C1 --> C11[Wahl zwischen 2 Übeln]
    A1 --> D1[Appetenz-Aversions-Konflikt]
    D1 --> D11[Wahl kann sowohl <br> angenehme als auch <br> unangenehme Seite haben]
    A --> A02[interpersonelle Konflikte]
    A02 --> A2[soziale Konflikte]
style A2 fill:#FFFFCC
```

## Formen des Konfliktes

```mermaid
graph TD
    A[Formen des Konflikte <br> Ralf Dahrendorf] --> B[manifester Konflikt]
    A --> C[latenter Konflikt]
    A --> D[umgeleiteter Konflikt]
```

## Normen- u. Interessenkonflikte

```mermaid
graph TD
    A[Konflikte] --> B[Normenkonflikt]
    B --> B1[unklare/fehlende <br> Normen]
    B --> B2[zahlreiche/einschränkende <br> Normen]
    B --> B3[widersprüchige <br> Normen]
    A --> C[Interessenkonflikt]
    C --> C0[unterschiedliche Interessen <br> zwischen Gruppen]
    C0 --> C1[Zielkonflikt]
    C0 --> C2[Beurteilungskonflikt]
    C0 --> C3[Verteilungskonflikt]
    C0 --> C4[Beziehungskonflikt]
```

## Kulturbedingte Unterschiede in der Kommunikation

```mermaid
graph TD
    A[Bedeutung der interkulturellen Kommunikation <br> in der modernen Gesellschaft] --> B[Globalisierung]
    A --> C[Migration]
    A --> D[lange <br> Auslandsaufenthalte]
    A --> E[globaler <br> Massentourismus]
```


```mermaid
graph TD
    A[Konfliktbereiche interkultureller Kommunikation] --> B[verschiedene Wert- <br> u. Normvorstellungen]
    A --> C[unterschiedliche <br> Sexualmoral]
    A --> D[unterschiedliche <br> Genderkulturen]
    A --> E[kulturspezifische <br> Sprache]
    A --> F[emotionale <br> Befindlichkeiten]
    A --> G[nonverbales <br> Ausdruckverhalten]
    B --> B1[Bsp. Generationenkonflikten]
    C --> C1[Bsp. Geschlechtsverkehr <br> vor der Heirat]
    D --> D1[Rolle von <br> Mann u. Frau]
    E --> E1[Sprache als <br> Informationsaustausch <br> oder als ästhetisches <br> Ausdrucksmittel]
    F --> F1[Lächeln als <br> Emotion oder <br> aus Höflichkeit]
    G --> G1[persönliche Distanz]
```




```mermaid
graph TD
    A[Strategien für <br> interkulturelle <br> Kommunikation] --> B[Aneignung von Wissen]
    A --> C[Offenheit für das Andere]
    A --> D[Bereitschaft <br> das Andere <br> zu akzeptieren]
    A --> E[Angstfreiheit <br> vor <br> dem Fremden]
    A --> F[Akzeptieren <br> von Unsicherheiten]
    A --> G[Mut zur <br> Neuorientierung]
    B --> B1[sich informieren <br> um Fehler <br> zu vermeiden]
    C --> C1[Ethnozentrismus]
    D --> D1[Separation u. Assimilation <br> verhindern interkulturelle <br> Kommunikation]
    E --> E1[Entwertung anderer <br> Kulturen ausbremsen]
```
