# Soziales Handeln


```mermaid
graph TD;
  A[Kommunikation]-->B[Erwartungen];
  B-->D[Reaktion];
```

**Film**: [Good Vibrations - Récréation](https://educ.arte.tv/program/good-vibrations-recreation) auf Educ’Arte. Soziale Interaktion, soziale Kommunikation, soziales Verhalten, soziales Handeln.

1. Inwiefern ist dies ein Beispiel für *soziale Interaktion* ?
2. Welche Arten (Beispiele) von *sozialer Kommunikation* finden sie hier?
3. Ist das ein Beispiel für *soziales Handeln*?
4. Verwenden Sie das Video, um die folgende Gedankenkette zu erklären : `Kommunikation --> Erwartung --> Reaktion `
