# Einführung in den historischen Materialismus

![QR code or link http://www.edulink.lu/jdip](../qr-codes/jdip-einfuhrung-in-den-historischen-materialismus.png)

## A. Text

Die Rolle der Industrialisierung nimmt in der Theorie des historischen Materialismus von Karl Marx einen zentralen Platz ein. Marx argumentierte, dass die Industrialisierung eine treibende Kraft des sozialen Wandels sei und den Übergang zur kommunistischen Gesellschaft ermögliche. Durch die Industrialisierung wurden traditionelle handwerkliche Produktionsweisen durch Fabriken und Maschinen ersetzt, was zu einer Konzentration des Produktionsprozesses und zur Entstehung einer neuen Klassengesellschaft führte. 

In dieser neuen Gesellschaft teilen sich zwei Hauptklassen die Bühne: die Kapitalisten und die Arbeiter. Die Kapitalistenklasse besitzt die Produktionsmittel und profitiert von der Arbeit der Arbeiterklasse, die gezwungen ist, ihre Arbeitskraft zu verkaufen, um ihren Lebensunterhalt zu verdienen. Diese Ausbeutung der Arbeiterklasse ist laut Marx eine unvermeidbare Konsequenz des kapitalistischen Systems, das auf der Maximierung des Profits basiert. 

Marx' Theorie des historischen Materialismus unterscheidet sich grundlegend von der idealistischen Philosophie Hegels. Während Hegel den Geist und die Ideen als treibende Kräfte des gesellschaftlichen Wandels betrachtete, argumentierte Marx, dass die materiellen Bedingungen und die ökonomischen Verhältnisse die Haupttriebfedern der Geschichte seien. Marx entwickelte den historischen Materialismus, der auf einer materialistischen Analyse der Gesellschaft und dem Klassenkampf basiert. 

Die Industrialisierung und ihre Auswirkungen auf die soziale Struktur sowie die Gegensätze zwischen Hegel und Marx sind zentrale Themen in der marxistischen Theorie und bieten einen Einblick in die Dynamik des sozialen Wandels und der kapitalistischen Gesellschaft. 


## B. Fragen

1. Warum spielt die Industrialisierung eine zentrale Rolle in der Theorie des historischen Materialismus? (12 P.) 
2. Wie definiert Marx die soziale Spaltung, die durch die Industrialisierung entsteht, und welche Klassen sind daran beteiligt? (8 P.) 
3. Was sind die Hauptunterschiede zwischen Hegels Ansatz zur Geschichte und Marx' historischem Materialismus? (12 P.) 


## C. Vorschläge

### C.1. Vorschläge zu Frage 1

```mermaid
graph TD
    A[Rolle der Industrialisierung <br> in der Theorie <br> des historischen Materialismus] --> A1[Aufteilung der <br> Gesellschaft <br> in Klassen]
    A --> A2[Neue Produktionsweise] 
    A2 --> A21[Handwerk <br> abgewertet]
    A --> A3[Kapitalisten u. <br> Arbeiter]
    A3 --> A31[Kapitalisten : <br> Profit]
    A3 --> A32[Arbeiter : <br> abhängig vom Lohn]
    A31 --> A311[mehr Macht]
    A32 --> A321[Ausbeutung]
```

<br>

<br>

<br>

```mermaid
graph TD
    A[Rolle der Industrialisierung <br> in der Theorie <br> des historischen Materialismus] --> A1[Wertewandel u. <br> wirtschaftlischer Wandel]
    A --> A2[Produktionsweise kann <br> zu soziale Ungleichheit führen]
    A --> A3[Mensch ist von <br> der Arbeit abhängig]
```
<br>

<br>

<br>


```mermaid
graph TD
    A[Rolle der Industrialisierung <br> in der Theorie <br> des historischen Materialismus] --> A1[Verschwinden <br> handwerklicher <br> Berufe]
    A --> A2[Entstehung einer <br> Arbeiterklasse]
    A --> A3[Kapitalisten]
```

<br>

<br>

<br>


```mermaid
graph TD
    A[Rolle der Industrialisierung <br> in der Theorie <br> des historischen Materialismus] --> A1[Entfremdung der Natur]
    A --> A2[Handwerk verliert <br> an Status]
    A --> A3[Arbeiter gezwungen <br> ihre Arbeit <br> zu verkaufen]
    A3 --> A31[abhängig] 
```
<br>

<br>

<br>


### C.2. Vorschläge zu Frage 2

```mermaid
graph TD
    A[Enstehung einer sozialen Spaltung <br> durch die Industrialisierung] --> A1[2 Klassengesellschaft : <br> Bourgeoisie/Kapitalisten <br> Proletariat/Arbeiter <br> Ausbeutung u. Ungleichheiten ]
    A --> A2[Kapitalisten : <br> besitzen Produktionsmittel <br> maximisieren Profit <br> <br> Proletarier <br> Arbeiten/Überleben] 
    A --> A3[wirtschaftliches System <br> beruht auf <br> ungleiche Verteilung <br> der Macht]
```

<br>

<br>

<br>

```mermaid
graph TD
    A[Enstehung einer sozialen Spaltung <br> durch die Industrialisierung] --> A1[Neue Produktionsweise <br> führt zu <br> Klassengesellschaft : <br> Arbeiter u. Kapitalisten]
    A --> A2[Kapitalisten : <br> <br> Besitz von Produktionsmitteln <br> <br> profitieren von <br> billigen Arbeitskräften <br> <br> besseres Ansehen <br> in der Gesellschaft]
    A --> A3[Arbeiter : <br> abhängig von <br> den Kapitalisten]
```

<br>

<br>

<br>

```mermaid
graph TD
    A[Enstehung einer sozialen Spaltung <br> durch die Industrialisierung] --> A1[Klassengesellschaft : <br> Arbeiter u. Kapitalisten <br> Arm u. Reich]
    A --> A2[kapitalistische System : <br> arme werden ärmer <br> reiche werden reicher]
    A --> A3[Arbeiter : <br> Arbeit/Überleben <br> <br> Kapitalisten : <br> Profit maximieren]
```

### C.3. Vorschläge zu Frage 3

```mermaid
graph TD
    A[Hauptunterschiede zwischen den <br> Ansichten von Hegel und Marx] --> A1[Hegel : <br> Geist u. Idee <br> ist treibende Kraft <br> für den Wandel <br> der Gesellschaft]
    A --> A2[Marx : <br> materielle Bedingungen <br> ist Hauptantriebsquelle <br> materialistische Analyse <br> der Gesellschaft]
    A --> A3[Wandel der Gesellschaft wird erreicht : <br> <br> durch Verändern des Bewusstseins - Hegel <br> <br> durch Verändern der Produktionsverhältnisse - Marx]
```

