# Migration 

1. Grundbegriffe
2. Migration als Prozess
3. Immigration und Integration
4. Transnationale Ansätze

## A. Grundbegriffe Migration als Prozess Immigration und Integration Transnationale Ansätze

- **Definition**: [Migration](https://www.bpb.de/kurz-knapp/lexika/glossar-migration-integration/270614/migration/) 
  - räumliche Bewegung von Menschen, längerfristige Verlagerung des Lebensmittelpunktes, über eine administrative Grenze hinweg
  - Definition der UN: 
    - *temporäre Migration*: 3-12 Monate am Zielort
    - *dauerhafte Migration*: mehr als 1 Jahr am Zielort
  - Ziel der Migration: 
    - bessere Chancen auf Bildung und Arbeit
    - Folge der Ausübung/Androhung von Gewalt
- [3 Kriterien](../mermaid/migration_3_kriterien_beschreibung.md) die zur Beschreibung der Migrationsbewegung benutzt werden:
  - *Raum*
  - *Dauer*
  - *Motive*
- [Push-Faktor und Pull-Faktor](../mermaid/migration_push_pull_faktoren_klassifikation.md)​
  - Push-Faktor (Druckfaktoren): sind die Faktoren des Heimatortes/Heimatlandes der Migranten, die zur *Emigration*/Auswanderung zwingen 
  - Pull-Faktor (Sogfaktor): sind die Faktoren des Aufnahmeortes/Aufnahmelandes der Migranten, die zur *Immigration* motivieren
- Kettenmigration (chain migration)​
  - **Definition**: [Kettenmigration](https://www.bpb.de/kurz-knapp/lexika/glossar-migration-integration/270587/kettenmigration/)
    - Migranten folgen bereits abgewanderten Verwandten/Bekannten ins Zielgebiet
    - erhalten Informationen über soziale Beziehungen/Netzwerke
    - soziale Beziehungen/Netzwerke vermindern finanzielle u. soziale Kosten der Migration 
    - Entwicklung stabiler Migrationsbeziehungen zwischen verschiedenen Ländern
- Pioniermigranten​
  - **Definition**: [Pioniermigration](https://www.bpb.de/kurz-knapp/lexika/glossar-migration-integration/270618/pioniermigranten/)
    - etablieren Kettenwanderungen indem sie Informationen über Chance u. Gefahren an nachfolgende Migranten geben
    - [*Chancen* u. *Gefahren* der Migration](../mermaid/migration_chancen_gefahren_der_migration.md)
- Migrationsnetzwerke
  - Entstehung eines *Migrationssystems*
  - erlaubt es die *erwarteten Gewinne* der Migration sicher erscheinen zu lassen
  - [*Migrationskosten*](../mermaid/migration_migrationskosten.md) bestehen aus:
    - Reisekosten
    - Informations u. Suchkosten
    - Opportunitätskosten
    - psychische Kosten


<div class="center">

```mermaid
graph TD;
    A0[Kettenmigration] -->|besteht aus <br> folgenden Elementen|A
    A[Migranten]-->|folgen|B[Pioniermigranten]
    B -->|und bilden|C[Migrationsnetzwerke]
    C -->|bestehen aus|D[interpersonellen Bindungen]
```
</div>

## B. Migrations als Prozess

- [3 Phasen des Migrationsprozesses](../mermaid/migration_3_phasen_migrationsprozess.md):
  1. Motivbildung
  2. Migration
  3. Eingliederung in die Aufnahmegesellschaft

## C. Immigration

```mermaid
graph TD;
    A[Gefühle gegenüber Fremden] -->|geprägt durch|B[Ethnozentrismus];
    B --> B1[negative Bewertung];
    B --> B2[Versuch zu verstehen <br> ohne Bewertung];
    B1 --> C[Misstrauen anderer <br> Sitten, Bräuche, <br> Gewohnheiten ...];
    B2 --> C;
    A --> |geprägt durch|D[Kulturrelativismus];    
    D --> D1[Kulturen werden <br> als gleichwertig <br> angesehen];
    D --> D2[Normen u. Werte <br> von anderen Kulturen <br> verstehen];
    D1 --> E[hat Vor- u. Nachteile];
    D2 --> E;
    E -->|Vorteile| E1[fördert Toleranz];
    E -->|Nachteile| E2[keine Anerkennung <br> universellen Wertesystem];
style B fill:#99CCFF
style D fill:#99CCFF
click B callback "https://www.bpb.de/kurz-knapp/lexika/politiklexikon/17422/ethnozentrismus/"
click D callback "https://www.bpb.de/themen/kriege-konflikte/dossier-kriege-konflikte/504301/kulturrelativismus/"
```


```mermaid
graph TD;
    A[verschiedene Integrationsmodelle] -->B1[Assimilationstheorie]
    B1 -->|inspiriert durch| B11[sozialwissenschaftliche <br> Evolutionstheorie]
    B11 --> B12[Prozess wo am <br> Ende die vollständige <br> Anpassung steht]
    B12 --> B13[Eigenschaften des <br> Herkunftslandes verschwinden]
    B13 -->|Kritik|B14[ethnozentristische <br> Haltung <br> u. <br> Anpassung an <br> Kerngesellschaft]
    A --> B2[Modell des <br> ethnischen <br> Pluralismus] 
    B2 -->|ausgelöst durch| B21[Bürgerrechtsbewegung]
    B21 -->|Einhaltung von| B22[ethnische Besonderheiten]
    B22 --> |Schutz|B23[ethnischer <br> u. kultureller <br> Unterschiede]
    B23 -->|Kritik|B24[Gemeinschaften werden <br> als homogene Gruppen <br> angesehen]
style B1 fill:#99CCFF
style B2 fill:#99CCFF
```

## Deutschsprachige Migrationsforschung

```mermaid
graph TD;
    A[Migrationsforschung] -->B[US-amerikanische <br> Soziologie <br> Assimilationstheorie]
    B --> C[deutschsprachige Migrationsforschung <br> 70er u. 80er]
    C --> D[handlungstheoretischer Modell <br> Hartmut Esser]
    D -->|orientiert sich nach| E[kognitiven Theorie <br> des Handelns u. Lernens <br> von Individuen]
    E --> E1[Handlung <br> = <br> rationale Entscheidung]
    E --> E2[individuelle Nutzung <br> maximisieren]
    E1 -->|verbunden mit der| F[Migration]
    E2 --> F
    F --> G[Prozess der Assimilation <br> hat 4 Dimensionen]
    G --> G1[kulturelle Assimilation]
    G1 -->|Erlernen von| G11[Wissen, Sprache, ...]
    G --> G2[strukturelle Assimilation]
    G2 -->|Übernahme von| G21[Rechten, Statuspositionen, <br> sozialer Aufstieg ...]
    G --> G3[soziale Assimilation]
    G3 -->|Entstehung| G31[sozialer Beziehungen, Netzwerke, <br> interethnische Freundschaften/Heiraten]
    G --> G4[emotionale Assimilation]
    G4 -->|Übernahme von| G41[Wertehaltung, Loyalität, <br> Identifikation mit Aufnahmeland]
    G11 --> H[kausaler Zusammenhang <br> zwischen den <br> 4 Dimensionen]
    G21 --> H 
    G31 --> H 
    G41 --> H 
    H -->|bestimmen den| I[Grad der gesellschaftlichen Integration des Individuums]
    I -->|Enstehung von|J[4 Möglichkeiten der Sozialintegration]
    J --> J1[Mehrfachintegration]
    J1 --> J11[hohe Bildung <br> der Eltern]
    J1 --> J12[keine <br> materiellen <br> Sorgen]
    J --> J2[Segmentation]
    J2 -->|Etablierung| J21[ethischer Gruppen <br> als gesellschaftliche <br> Einheit]
    J --> J3[Assimilation]
    J3 --> J31[Verschwinden der <br> systematischen <br> Unterschiede]
    J --> J4[Marginalität]
    J4 --> J41[Totalexklusion]
style D fill:#99CCFF
style E fill:#CCE5FF
```
### Kritik an Essers Theorie

1. Probleme der Integration : mangelnde Anpassung der Migranten 
2. Nichtbeachtung von :
  1. strukturelle Ungleichheiten
  2. Diskriminierung der Exklusion durch dominante Gruppen 
3. Entweder-Oder (Segmentation oder Assimilation) Entscheidung der Migranten

## Fragen zum Thema 

1. Definiton: Migration
2. Drei Kriterien die in der Forschung gebraucht werden um die Migration genau und sachlich zu beschreiben.
3. Geben Sie 5 Bereiche mit jeweils 3 Beispielen von Push-Faktoren.
4. Geben Sie 4 Bereiche mit jeweils 3 Beispielen von Pull-Faktoren.
5. Definition: Kettenmigration
6. Wie können soziale Netzwerke den Migrationsvorgang vereinfachen?
7. Definition: Pioniermigranten
8. Identifizieren Sie die drei Phasen der Migrations.
9. Was meint Shmuel Eisenstadt mit *Motivbildung*?
10. Was meint Shmuel Eisenstadt mit *Migration*?
11. Was meint Shmuel Eisenstadt mit *Eingliederung in die Aufnahmegesellschaft*?
12. Definition: Ethnozentrismus
13. Was ist mit *Assimilation* gemeint?
14. Was ist mit *ethnischem Pluralismus* gemeint?
15. Welche 4 Dimensionen unterscheidet Hartmud Esser im Prozess der Assimilation?
16. Nennen Sie die 4 Typen der Sozialintegration nach Hartmud Esser.
17. Was ist mit *kultureller Assimilation* gemeint?
18. Was ist mit *struktureller Assimilation* gemeint?
19. Was ist mit *sozialer Assimilation* gemeint?
20. Was ist mit *emotionaler Assimilation* gemeint?


## Bibliograpie

- [Glossar Migration – Integration – Flucht & Asyl (Bundeszentrale für politische Bildung)](https://www.bpb.de/kurz-knapp/lexika/glossar-migration-integration/)
- [Integrationstheorien und ihr Einfluss auf Integrationspolitik](https://www.bpb.de/themen/migration-integration/kurzdossiers/integrationspolitik/269373/integrationstheorien-und-ihr-einfluss-auf-integrationspolitik/)
