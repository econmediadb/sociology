# The Young Karl Marx

```mermaid
graph TD;
    A[Begegnung zwischen Engels u. Marx] -->A1[Engels bewundert die <br> Klarheit von Marx' <br> materiellem Denken];
    A --> A2[Marx bewundert <br> Engels' Studie <br> über die englische <br> Arbeiterklasse]
    A1 --> B[gemeinsam entwickeln sie das neue Denken <br> <br> beeinflusst von Pierre Proudhon <br> <br> Marx und Engels werden von den Junghegelianern abgestoßen]
    A2 --> B
    B --> C[Marx u. Engels werden aus Frankreich ausgewiesen <br> <br> fliehen nach London]
    C -->|treten der| D[sozialistischen Bruderschaft 'League of the Just']
    D --> |durch den Bruch mit Proudhon|E[Rekonstruktion der 'League of the Just' als Kommunistischer Bund <br> <br> basiert auf die Doktrin des Konflikts zwischen Proletarier und Bourgeoisie]
```
![Jenny Marx, Karl Marx, Friedrich Engels](https://static01.nyt.com/images/2018/02/23/arts/23youngkarlmarx2/merlin_133995644_7469d8e2-fef2-4223-8bcd-76567fd1ac9c-superJumbo.jpg)

![Pierre Proudhon](https://fr.web.img6.acsta.net/r_1280_720/pictures/17/06/30/16/29/210758.jpg)


<!--
Marx strahlt, als er vorgestellt wird; er erinnert sich an den jungen Friedrich von einer früheren Begegnung, stolzierend und hochmütig, als ob er den Klassenkampf erfunden hätte. Der ruppige junge Schläger gerät mit dem arroganten Jüngling aneinander. Aber das Eis bricht: Engels bewundert die Klarheit von Marx' materiellem Denken; Marx ist ein großer Fan von Engels' bahnbrechender Studie über die englische Arbeiterklasse. Gemeinsam atmen sie das neue Denken ein, das in der Luft liegt, Ideen, für die Pierre Proudhon  mitverantwortlich ist. Von den Franzosen ausgewiesen, flieht Marx mit Engels nach London, wo sie eingeladen werden, der sozialistischen Bruderschaft League of the Just beizutreten und ihrer evangelikalen Bewegung intellektuelle und methodologische Strenge zu verleihen. Doch der Bruch mit Proudhon ermutigt beide, und Engels erklärt schließlich auf dem verblüfften Jahreskongress, dass der Bund der Gerechten als Kommunistischer Bund rekonstituiert werden soll.

Bis seine Partnerschaft mit Engels gefestigt ist - und der Zugang zu einem Teil des Familienvermögens von Engels gesichert ist - kämpft Marx mit Terminen, verärgert Redakteure und lebt mit seiner Frau Jenny (Vicky Krieps) und ihrer kleinen Tochter in ärmlichen Verhältnissen. Die Familie wird von Inkassobüros und auch von der Polizei belästigt, die von Köln über Paris nach Brüssel reist, da Marx' politische Agitation ihn als gefährlichen Subversiven brandmarkt.

Er ist auch in eine Reihe unbeständiger Allianzen mit bedeutenden Persönlichkeiten der europäischen Linken verwickelt, insbesondere mit Wilhelm Weitling (Alexander Scheer), einem bombastischen Redner, dessen Ideen bestenfalls schwammig klingen, und Pierre-Joseph Proudhon (der wunderbare Olivier Gourmet), einem gutmütigen Philosophen, der der fleißigste Anarchist im Showgeschäft zu sein scheint. Diese Namen sind heute ziemlich obskur, während der von Marx immer noch mit einem -ismus verbunden ist, wie ramponiert auch immer. (Zählt das als Spoiler?) Er und Engels inszenieren sowohl einen theoretischen als auch einen organisatorischen Coup, indem sie utopische Nostrums über die Brüderlichkeit der Menschen durch eine Doktrin des Konflikts zwischen Proletarier und Bourgeoisie ersetzen. Der Rest ist Geschichte, blutig und umstritten und noch nicht zu Ende.

Die Übernahme einer schwerfälligen Arbeitergruppe namens "Bund der Gerechten" durch Marx und Engels ist eine Vorahnung der taktischen Rücksichtslosigkeit, die für die kommunistischen Parteien des 20. Jahrhunderts charakteristisch werden sollte. Um es in der Sprache des Kapitalismus des 21. Jahrhunderts auszudrücken: Die Gründer des Kommunismus sind Umstürzler und Erneuerer, im Besitz der unternehmerischen Verbissenheit und des selbstgerechten Eifers, die erforderlich sind, um die Welt ihrem Willen zu unterwerfen.

Jenny Marx ist eine rebellische preußische Aristokratin; Engels' Frau Mary Burns (Hannah Steele) ist eine rebellische irische Fabrikarbeiterin. Die beiden Frauen sorgen für eine gewisse Zentrifugalkraft, die die Geschichte weg von der Politik hin zu Herzensangelegenheiten und Herzensangelegenheiten zieht. Aber genau aus diesem Grund zeigen sie auch die Lücken in der aufkommenden Theorie ihrer Männer über Ungleichheit und Ausbeutung auf. Jenny und Mary unterstützen Karl und Friedrichs Arbeit von ganzem Herzen, aber auch innerhalb der Parameter einer Arbeitsteilung, die außerhalb der Reichweite ihrer Politik zu liegen scheint. Sie räumen das Geschirr ab, hüten die Kinder, verteilen Flugblätter und hängen Transparente auf - sie sind Teil der Geschichte, aber auch an deren Rande.

In dem Film wird gezeigt, dass Marx und Engels von den "Junghegelianern", einer modischen Gruppe subjektiv orientierter Denker, für die sie wenig Respekt und Geduld aufbrachten, abgestoßen werden. Dies inspirierte beide zu der Schrift Die heilige Familie oder Kritik der kritischen Kritik. Laut der Los Angeles Times "ist dies die Art von Film, in dem wir nicht nur von den Junghegelianern hören, sondern auch einige von ihnen treffen, die sich in einem Zeitungsbüro im Köln des Jahres 1843 verbal mit Marx streiten". Die Rivalität setzt sich während des gesamten Films fort.

Darüber hinaus sieht Marx weitere Konkurrenten auf der europäischen Linken, allen voran vielleicht Wilhelm Weitling, gespielt von Alexander Scheer. Marx und Engels sehen diese Rivalen als bloße "intellektuelle Touristen", die nicht bereit sind, den Kern der Sache klar zu benennen: den Konflikt zwischen der Bourgeoisie und dem Proletariat. Im Film kommt auch der berühmte russische Anarchist Michail Bakunin vor, den Noam Chomsky als denjenigen identifiziert hat, der die Entwicklung der Kommandowirtschaften in Ost und West im 20.


Der Film zeigt in hervorragender Weise die Rolle von Jenny von Westphalen, der Lebensgefährtin von Marx, einer privilegierten Preußin, die zum täglichen Überleben der Familie beitrug und auch an der Redaktion des Kommunistischen Manifests mitwirkte. Sie wird von Vicky Krieps gespielt und wird zu einer radikalen Schlüsselfigur, die Marx dazu inspiriert, zu erkennen, wie wichtig es ist, den "vieux monde craquer", den "Riss in der alten Welt", wie Annie Julia Wyman sagt, klar zu formulieren. Im Film sehen wir Marx, der von Köln über Paris nach Brüssel reist, als einen komplizierten Menschen: Er liebt seine Frau und seine Kinder, ist aber unorganisiert, kommt mit Rechnungen zu spät und neigt zu langen Nächten mit exzessivem Rauchen, Trinken und Streit. Jenny Marx erweckt den Film zum Leben, ebenso wie Marx.

-->


## Source

[The Young Karl Marx review – intelligent communist bromance](https://www.theguardian.com/film/2017/feb/12/the-young-karl-marx-review-communist-bromance-raoul-peck)

[Review: In ‘The Young Karl Marx,’ a Scruffy Specter Haunts Europe](https://www.nytimes.com/2018/02/22/movies/the-young-karl-marx-review.html)

[*Le jeune Karl Marx* (Director: Raoul Peck; Stars: August Diehl, Stefan Konarske, Vicky Krieps, Olivier Gourmet, Hannah Steele)](https://archive.org/details/the-young-karl-marx-2017-720p-eng-subs-le-jeune-karl-marx)

[“The Young Karl Marx” Is an Urgent Reminder to Revisit Marx’s Ideas](https://truthout.org/articles/the-young-karl-marx-is-an-urgent-reminder-to-revisit-marxs-ideas/)

[Karl Marx: Thoroughly Revised Fifth Edition](https://www.a-z.lu/discovery/fulldisplay?docid=alma9920914651107251&context=L&vid=352LUX_BNL:BIBNET_UNION&search_scope=DN_and_CI_UCV&tab=DiscoveryNetwork_UCV&lang=fr)
