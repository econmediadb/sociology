# Gesellschaftstheorien

## Schwerpunkte der Gesellschaftstheorien

- Gesellschaftstheorie 
  - untersuchen/analysieren soziale Probleme
  - Bsp. historische Materialismus (Marx und Engels) , ....
- System- und Handlungstheorien vs. Gesellschaftstheorien
- Mithilfe dieser Theorien können wir:
  - sozialen Wandel vorhersagen
  - Wirklichkeit gezielt ändern
  - gesellschaftliche Fehlentwicklungen korrigieren
- Machtverhältnisse in einem Staat

## Die Theorie des historischen Materialismus

- Ausgangspunkt der Theorie des historischen Materialismus 
  - Industrialisierung
  - Analyse der Entwicklung wirtschaftlicher Produktionsverhältnisse
  - Beziehung zwischen den Besitzern der Produktionsmittel und den nicht besitzenden Arbeitern
 - Marx Hauptinteresse : Gesellschaften, die eine kapitalistische Produktionsweise haben
 - Karl Marx: [siehe *Le jeune Karl Marx*](https://archive.org/details/the-young-karl-marx-2017-720p-eng-subs-le-jeune-karl-marx)
   - 1818-1883
   - studierte Jura in Bonn u. Berlin
   - Chefredakteur der Rheinischen Zeitung (Köln) und Herausgeber der Deutsch-französischen Jahrbücher
   - Ausweisung aus Frankreich, siedelt nach Brüssel
   - Schrieb zusammen mit Engels das *Kommunistische Manifest* (1848)
   - Ausweisung nach Frankreich (Teilnahme an Aufständen in Brüssel)
   - Ausweisung nach London und Gründung der *Internationalen Arbeiterassoziation*
- Positives Menschenbild:
  - aktives handelndes Individuum
    - Arbeit
    - Interaktion
  - Fähigkeit seine Umwelt zu verändern

## Die Kernaussagen des historischen Materialismus 

- Ausgangspunkt für die marxistische Gesellschaftstheorie: *theoretische Idealismus* von Hegel
  --> Hegel sieht Ideen/Ideale als Ursache für die Verhältnisse in der Menschheitsgeschichte an
- Geschichte ist für Marx (und auch für Hegel):
  1. Ursprünglich lebte die Menschheit mit der Natur in Einklang: Menschen setzten sich durch ihre *produktive Arbeit* mit der *Eigengesetzlichkeit der Natur* auseinander
  2. Im Laufe der Zeit änderten sich die Produktionsverhältnisse: Zustand der *Entfremdung* - Menschheit der Gegenwart befindet sich in einem Gegensatz zur Natur.
- Marx verwendet die *Methode der Dialektik*:
  1. Denken in Gegensätzen: 
  2. Erlaubt fortschreitende Veränderungen von Gesellschaften und gesellschaftliche Widersprüche aufzudecken 
  3. Aufgrund dieser Gegensätze entfaltet sich auf einer höheren Ebene eine Synthese des Sozialismus als Übergangsstadium zum Kommunismus 
  4. Wenn alle Gegensätze aufgehoben sind, ist das Endziel, der Kommunismus, erreicht.
- Produktionsverhältnisse der Gesellschaft:
  - Basis jeder menschlichen Gesellschaft 
  - Gesellschaften wandeln sich aufgrund ökonomischen Wandelns und Klassenkämpfen, in denen ökonomische Interessenkonflikte ausgetragen werden. (dies ist im Gegensatz zu Hegel der folgendes behauptet: die *Gesellschaft wnadelt sich durch Ideen und Ideale*)
- Kernaussagen des historischen Materialismus :
  1. Alles Leben geht auf Materie zurück
  2. Geschichte ist vom Menschen gemacht
  3. Die historische Entwicklung ist durch die Entfaltung der Produktivkräfte mehr oder weniger festgelegt
  4. Arbeit und Produktion sind die Lebensbasis der Menschen 
  5. Die Produktionsweise charakterisiert die jeweilige Gesellschaftsform
  6. Das Bewusstsein und die Sozialverhältnisse werden bedingt durch die ökonomischen Verhältnisse 
  7.  Die Veränderung der Bewusstseinsformen erfolgt in einem dialektischen Prozess

## Arbeit und Produktion als Lebensbasis

- Arbeit: charakteristisch für das menschliche Dasein
  - nötig im Umgang mit der Natur: dient dazu Nahrungsmittel zu beschaffen und zu erzeugen (zu produzieren)
  - menschliche Aktivität die zielbestimmt/zweckmäßig/planmäßig zur Erbringung von Leistung erfolgt 
  - dient der Erzeugung/Beschaffung/Umwandlung/Verteilung/Benutzung von ideellen und materiellen Produkten 
 - Marx interessiert sich um den abhängigen Arbeitnehmer der für seine Tätigkeit von seinem Arbeitgeber Lohn erhält (es geht nicht um den Selbstständigen/Unternehmer/Manager/..)

 ## Produktivkräfte und Produktionsverhältnisse 

 - Menschen unterliegen in der Gestaltung ihrer Arbeit vielfältigen Zwängen:
   - Produktivkräfte/Arbeitskraft: Mittel und Möglichkeiten materieller, technischer und wissenschaftlicher Art, Fähigkeiten die zur Bewältigung der wirtschaftlichen Produktion zur Verfügung stehen (z.Bsp. Roboter, Hard- und Software, Qualifikation, Erkenntnisse der Wissenschaften, ...)
   - Produktionsverhältnisse: gesellschaftliche Organisation des Produktionsprozesses und der Verteilung der Güter sowie die Frage der Verfügung der Produktionsmittel
- Merkmale von Produktionsverhältnissen:
  - Eigentumsverhältnisse: Wer verfügt über die Produktionsmittel?
  - Herrschaftsverhältnisse: Wer vefügt über die Entscheidungsbefügnisse?
  - Arbeitsverhältnisse: Wie wird gearbeitet? In Gruppen, freiwillige oder zwangsweise Zusammenarbeit?
  - Konsumationsverhältnisse: Wie werden die Wirtschaftsgüter verbraucht?
  
## Die Struktur der Wirklichkeit

- Produktivkräfte u. Produktionsverhältnisse sind strukturbestimmend für die Gesellschaft (bilden die Basis)
- Geschichte und Kultur werden :
  - bestimmt durch die wirtschaftlichen verhältnisse der Gesellschaft 
  - nicht durch Ideen und geistiger Kräfte der Menschen 
- ideologische Überbau wird durch ökonomische Verhältnisse bestimmt 
- mit der Veränderung der Basis verändert sich auch der Überbau 

## Entwicklung verschiedener Gesellschaftsformen

1. Urgesellschaft: Gemeineigentum an Grund und Boden 
2. Sklavenhaltergesellschaft: Privateigentum an Produktionsmitteln, zunehmende Arbeitsteilung und Tausch, zwei Klassen: Sklaven und Sklavenhalter 
3. Feudalgesellschaft: private Eigentum weltlicher und geistlicher Feudalherren an Grund und Boden sowie die Abhängigkeit der Bauern von den Grundherren
4. Kapitalismus: Zweiklassengesellschaft - Besitzende u. Nichtbesitzende 
5. Sozialismus: Produktionsweise in der das Kollektiveigentum zum Prinzip wird; Planwirtschaft, klassenlose Gesellschaft 
6. Kommunismus: klassenlose Gesellschaft 

## Gebrauchs- und Tauschwert 
 
- Kapital
- Kapitalismus
- Merkmale der kapitalistischen Wirtschaftsweise :
  - Privateigentum der Produktionsmittel
  - Eigeninteresse als Motiv
  - Gewinnorientierung
  - Reinvestition von Teilen des Gewinns
  - Wettbewerb zwischen Unternehmen
  - Akkumulation
- Kapitalist kann nur deshalb einen Mehrwert erzielen, weil er die Arbeitskraft und Arbeitszeit des Arbeitenden kauft
- Nach Marx ist ein *Produkt* für den Besitzer auf zweifache Weise wertvoll:
  - Gebrauchswert: Nützlichkeit des Produktes für eine Person
  - Tauschwert: Wert des Produktes beim Tauschen mit einer anderen Ware 
- Nach Marx besitzt die *menschliche Arbeitskraft* (u. die Arbeitszeit) auch:
  - Gebrauchswert 
  - Tauschwert
- Das Produkt und die Arbeitszeit sind also "Ware" und zwischen diesen beiden besteht eine Tauschbeziehung
 ## Kapitalistische Wirtschaftsverhältnisse

 - Mehrwert 
 - Ziel der kapitalistischen Produktion: 
   - Mehrwert erzielen
   - Gewinn/Profit zu erwirtschaften

## Aufgaben

1. Beschreiben Sie die Grundaussagen der Theorie des historischen Materialismus. 
2. Erläutern Sie anhand eines selbst gewählten Beispiels das Verständnis von Dialektik.
3. Beschreiben Sie die gesellschaftlichen Bedingungen, die Marx dazu veranlassten, seine Theorie des historischen Materialismus zu entwerfen.
4. Verdeutlichen Sie die Grundannahmen der Theorie des historischen Materialismus zu folgenden Stichworten: 
   - Arbeit
   - Produktivkräfte 
   - Produktionsverhältnisse 
   - Arbeitskraft 
   - Produktionsmittel
4. Erläutern Sie die Stichworte aus der vorherigen Frage anhand von Beispielen aus der heutigen Zeit.
5. Beschreiben sie die gesellschaftliche Struktur nach Karl Marx und erläutern Sie die Zusammenhänge anhand von Beispielen.
6. Begründen Sie, mit welchen Zukunftsprognosen zur gesellschaftlichen Entwicklung er sich irrte und welche Aussagen auch heute noch aktuell sind.
7. Erläutern Sie anhand von Beispielen die Merkmale der kapitalistischen Wirtschafts- und Gesellschaftsordnung auf der Grundlage der Analyse von karl Marx.
8. Bestimmen Sie die Begriffe Kapital, Tausch- und Gebrauchswert, Proletariat, Mehrwert und Kapitalakkumulation. Verdeutlichen Sie die Zusammenhänge anhand von Beispielen.
