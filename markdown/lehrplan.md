## Lehrplan (2GSO)


1. Einführung
   1. Grundbegriffe (Gesellschaft, System, Institution, ...)
   1. Der Gegenstand der Soziologie
1. Soziales Handeln
   1. Das Zusammenwirken von Menschen
      1. Die Begriffe _soziale Interaktion_ und _soziale Kommunikation_ ( [Aufgabe](https://gitlab.com/econmediadb/sociology/-/blob/main/markdown/applications/good-vibrations-recre.md) )
      1. Menschliches Zusammenleben als soziales Handeln
   1. Die Wertbezogenheit sozialen Handelns
      1. Soziale Werte
      1. Der Wandel von Werten
   1. Die Regelung des Zusammenlebens
      1. Soziale Normen als Verhaltensvorschrift
      1. Arten von Normen
      1. Soziale Rolle als Verhaltenserwartung
      1. Rollenkonflikte
   1. Das Erlernen des sozialen Verhaltens (in Absprache mit PEDAG)
      1. Der Begriff _Sozialisation_
      1. Sozialisation als lebenslanger Prozess
      1. Theorien der Sozialisation
1. Soziale Kontrolle und Abweichung
   1. Die Gewährleistung normengerechten Verhaltens
      1. Soziale Kontrolle als Überwachung
      1. Sanktionen als Überwachungsmaßnahmen
   1. Anpassung und Abweichung
      1. Sozialangepasstes Verhalten / Sozialabweichendes Verhalten
   1. Die Problematik abweichenden Verhaltens
      1. Die Norm als Beurteilungsmaßstab
      1. Die normorientierte Einschätzung abweichenden Verhaltens
      1. Theorie der Zuschreibung
   1. Sozialer Konflikt
      1. Der Begriff _sozialer Konflikt_
      1. Formen des Konfliktes
      1. Ursachen und Funktionen des sozialen Konfliktes
      1. Konfliktmanagement
1. Soziale Gruppe
   1. Die Gruppe als soziales Gebilde
      1. Der Begriff _Gruppe_
      1. Der Prozess der Gruppe
   1. Die Bedeutung von Gruppen
      1. Funktionen der Gruppe
      1. Das Konzept des sozialen Netzwerkes
      1. Gefahren einer Gruppe
   1. Arten von Gruppen
      1. Primär-und Sekundärgruppe
      1. Eigen-und Fremdgruppe
      1. Formelle und informelle Gruppe


## Lehrplan (1GSO)

1. Soziale Ungleichheit 
   1. Voraussetzungen und Entstehung sozialer Ungleichheit 
      1. Der Begriff „soziale Ungleichheit“ 
      1. Voraussetzungen der sozialen Ungleichheit 
   1. Soziale Ungleichheit 
      1. Theorien zur sozialen Ungleichheit (Marxismus, Funktionalismus, Dahrendorf, Bourdieu u.a) 
      1. Die geschichtliche Entwicklung sozialer Ungleichheit 
   1. Historischer Materialismus 
      1. Kernaussagen 
      1. Arbeit und Produktion, Produktivkräfte und Produktivverhältnisse, Struktur der Wirklichkeit 
      1. Kapital, Kapitalismus und kapitalistische Wirtschaftsverhältnisse 
   1. Ungleiche Lebensbedingungen 
      1. Bildung, Beruf und Beschäftigung, Einkommen, Vermögen und Macht 
   1. Soziale Beweglichkeit  
      1. Soziale Mobilität und soziale Reproduktion 
      1. Soziale Randgruppen und Stigmatisierung 
1. Sozialstruktur und soziale Schichtung 
   1. Die Sozialstruktur und der soziale Wandel 
      1. Sozialstruktur 
      1. Soziale Wandel 
      1. Praktische Anwendung 
   1. Sozialer Status und Prestige 
   1. Modelle der Sozialstruktur 
      1. Soziale Stände, Klassen und Kasten 
      1. Soziale Schicht und Schichtung 
      1. Neuere Schichtungsmodelle 
      1. Lebenslagen und soziale Lagen 
      1. Soziale Milieus und Lebensstile 
1. Migration (Syllabus) 
   1. Grundbegriffe 
   1. Migration als Prozess 
   1. Immigration und Integration 
   1. Transnationale Ansätze 

## Examen

[Questionnaires des examens de fin d'études](https://portal.education.lu/Services/Examens)
