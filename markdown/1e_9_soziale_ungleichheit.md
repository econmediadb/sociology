# Soziale Ungleichheit

## Der Begriff "soziale Ungleichheit"

- biologische und soziale Merkmale
- Bewertung
- soziale Ungleichheit
- Verteilungsungleichheit
- Chancenungleichheit

## Voraussetzungen der sozialen Ungleichheit

- Drei Voraussetzungen :
  1. erstrebenswertes "Gut"
  2. ungleiche Verteilung des wertvollen Guts in der Gesellschaft
  3. regelmäßige Weise ungleiche Verteilung aufgrund der Stellung von Menschen in der Gesellschaft

## Theorien zur Entstehung sozialer Ungleichheit

- Ungleichheit als naturgegeben, Sozialdarwinismus
- Ungleichheit als gottgewollt, Glaube und Tradition der europäischen Ständeordnung, Karma im Hinduismus
- Ungleichheit ist gesellschaftlich bedingt (18. Jahrhundert)
- Privateigentum als Ursache von sozialer Ungleichheit (Lorenz von Stein, Jean Jacques Rousseau, Klassenmodell von Karl Marx und Friederich Engels);
  1. *erstes Unheil* (nach Rousseau): Aufteilung des verfügbaren Bodens --> Vergrößerung des Reichtums kann nur auf Kosten von anderen stattfinden --> Herrschaft und Knechtschaft
  2. *zweites Unheil* (nach Rousseau): Einsetzung einer Obrigkeit um das Eigentum zu schützen --> Herrschende und Beherrschte
  3. *drittes Unheil* (nach Rousseau): Ausartung dieser Macht --> Herren und Sklaven
- Besitz von Produktionsmittel als Ursache von sozialer Ungleichheit (Karl Marx und Friederich Engels); zwei Klassen (Bourgeoisie und Proletariat)
- Bewertung als Ursache von sozialer Ungleichheit (Talcott Parsons)
- Soziale Normen und Sanktionen als Ursache von sozialer Ungleichheit (Ralf Dahrendorf)
- Qualifikation der Arbeitenden als Ursache von sozialer Ungleichheit (Gary Becker); Humankapitaltheorie
- Kampf um das Kapital als Ursache von sozialer Ungleichheit (Pierre Bourdieu); Chancenungleichheit
- Marktprozesse als Ursache von sozialer Ungleichheit (Adam Smith); Markttheoretische Ansätze

## Die gesellschaftliche Entwicklung sozialer Ungleichheit

1. Ständegesellschaft --> Geburt als Bestimmungsmerkmal
2. Klassengesellschaft --> Besitz als Bestimmungsmerkmal
3. Schichtgesellschaft --> Beruf als Bestimmungsmerkmal

## Ungleiche Lebensbedingungen

1. Bildung
   - Bildungsexpansion
   - Bildungsarmut
   - Exklusion
   - Bildungsverlierer
2. Beruf und Beschäftigung bzw. Erwerbtätigkeit
   - Arbeitslosigkeit
   - Qualifizierte, ausländische Mitbürger, ältere Arbeitnehmer
3. Einkommen
   - Wohlstandsvermehrung
   - Schere beim Einkommen
   - Armut
   - Aquivalenzeinkommen
   - relative Armut
   - Median
   - gefühlte Armut
   - Ausgrenzung und Diskriminierung
   - prekäre Lage
   - Niedriglohn
   - Billiglohn
4. Vermögen (besitz, Reichtum)
   - Finanzvermögen
   - reales Vermögen
   - reich 
   - Spitzeneinkommen
   - Privatvermögen
   - Wirkung der Globalisierung
   - Rückkehr des Proletariats
5. Macht 
   - Elite
   - Wissen ist Macht

6. Weitere Kriterien für die postindustrielle Gesellschaft:
   - Arbeitsbedingungen
   - Freizeitbedingungen
   - Gesundheitsbedingungen
   - Wohnbedingungen
   - regionale Bedingungen
   - Teilhabe am Sozialversicherungssystem   

## Soziale Beweglichkeit

### Sozialer Auf- und Abstieg

- soziale Mobilität: Bewegung/Wechsel von Menschen zwischen sozialen Positionen
- vertikale und horizontale soziale Mobilität
- sozialer Status
- freiwillige Bewegung
- gesellschaftlicher Strukturwandel
- strukturelle Mobilität
- offene Gesellschaft

### Soziale Randgruppe

- Exklusion
- soziale Ausschließung
- Ausschließung aus ökonomischen, kulturellen und politischen Prozessen
- Randgruppe
- soziale Minderheit


## Materialien

- Bildung in der BRD; 
- Thesen zur Bildungsarmut
- Soziale Herkunft von Studierenden in Deutschland
- Beruf und Beschäftigung
- Bevölkerung nach Lebensunterhalt
- Arbeitslosigkeit in Deutschland
- Entwicklung der Langzeitarbeitslosigkeit
- Langzeitarbeitslosigkeit
- Nedriglohnbeschäftigte in Deutschland
- Der Median
- Einkommen und Vermögen
- Entwicklung des Volkseinkommens
- Entwicklung der Löhne
- Einkommen von Vollzeit-Arbeitnehmer
- Nettogeldvermögen der Privathaushalte
- Armut in Deutschland
- Armutsgefährdung in Deutschland
- Betroffene von Armutsgefährdung

## Aufgaben

1. Bestimmen Sie den Begriff "soziale Ungleichheiten" und zeigen Sie an je einem Beispiel die beiden Ausprägungen sozialer Ungleichheit auf.
2. Erläutern Sie an einem Beispiel die Voraussetzungen, die gegeben sein müssen, damit eine Unterschiedlichkeit und ihre Bewertung als soziale Ungleichheit gelten.
3. Geben Sie einen Überblick über die verschiedenen Theorien der Entstehung sozialer Ungleichheit.
4. Beschreiben Sie vorindustrielle Gesellschaften hinsichtlich sozialer Ungleichheit.
5. Erläutern Sie an Beispielen Kriterien, in denen sich soziale Ungleichheit äußert.
6. Zeigen Sie soziale Ungleichheit in Ihren Land auf hinsichtlich:
   - der Bildung
   - des Berufes
   - des Einkommens
   - des Vermögens
7. Bestimmen Sie, was man unter Elite versteht, und beschreiben Sie, wie in Ihrem Land die Macht auf verschieden Bereiche verteilt ist.
8. Beschreiben Sie an je einem Beispiel "neue Ungleichheiten" der postindustriellen Gesellschaft.
9. Bestimmen Sie den Begriff "soziale Mobilität" und stellen Sie an einem Beispiel vertikale und horizontale Mobilität dar.
10. Erläutern Sie an zwei Beispielen, was die Soziologie mit Randgruppen meint.

## Quellen

Soziologie. Sylvia Betscher-Ott, Wilfried Gotthardt, Hermann Hobmair, Wilhelm Ott, Rosmaria Pöll.


