# Soziologie

## Syllabus

|Kapitel | Kapitel Buch | Seiten| Fragen |
|--- | --- | ---| ---| 
| **1. Soziale Ungleichheit** |   |   | |
| **a)** Voraussetzungen und Entstehung sozialer Ungleichheit | 9.1 | | |
| Der Begriff „soziale Ungleichheit“ | 9.1.1 | 278-280 | 1 |
| Voraussetzungen der sozialen Ungleichheit | 9.1.2 | 280-281 | 2 |
| **b)** Soziale Ungleichheit | | | |
| Theorien zur sozialen Ungleichheit (Marxismus, Funktionalismus, Dahrendorf, Bourdieu u.a) |9.1.3| 285-285 | 3 |
| | Marxismus im Detail| | |
| | 7.2.1 | 205-207 | 1 |
| | 7.2.2 | 207-208 |  | 
| | 7.2.3 |         | 2 |
| | 7.2.4 |         | 3 |
| | 7.2.6 Kapital und Kapitalismus | |  |
| | 7.2.7 | Profit zu erwirtschaften.“ (inklusiv) | | 4 |
| Die geschichtliche Entwicklung sozialer Ungleichheit | 9.1.4 | 285-286 | 4 |
| Ungleiche Lebensbedingungen | 9.2 | 286-297 | 5, 6, 7, 8 |
| Soziale Mobilität und soziale Reproduktion | 9.3.1 | 298-299 | 9 |
| Soziale Randgruppen und Stigmatisierung | 9.3.2 (Randgruppe) | 299-301 (Schema inklusiv) | 10 |
| | 3.3.3 (Stigma und Stigmatisierung) | 90-91 | 13, 14|
| **2. Sozialstruktur und soziale Schichtung** | | | |
| Die Sozialstruktur und der soziale Wandel (Praktische Anwendung) | 10.1 | 318-320 | 1, 2|
| Sozialer Status und Prestige | 10.2 | 320-323 | 3, 4, 5, 6, 7|
| Soziale Stände, Klassen und Kasten | 10.3.1 der Stand | 323-325 | 8, 9|
| | 10.3.2 Klassenmodelle | 325-326 | 10, 11|
| Soziale Schicht und Schichtung | 10.3.3 | 326-328 | 12 |
| Neuere Schichtungsmodelle | 10.4 (Definitionen Milieu und Lebensstil) | | |
| Lebenslagen und soziale Lagen | 10.4.1 | 332 | |
| Soziale Milieus und Lebensstile | 10.4.2 | 334-335 | 14, 15 |
| **3. Migration** | Syllabus | | |


## Schlüsselwörter

|Kapitel | Schlüsselwörter |
|--- | --- |  
| **1. Soziale Ungleichheit** |    |
| **a)** Voraussetzungen und Entstehung sozialer Ungleichheit |  |
| Der Begriff „soziale Ungleichheit“ | soziale Ungleichheit, Bewertung, biologische und soziale Merkmale |
| Voraussetzungen der sozialen Ungleichheit | Voraussetzungen für soziale Ungleichheiten, erstrebenswertes *Gut*, ungleiche Verteilung des Erstrebenswerten, ungleiche Verteilung aufgrund der Stellung |
| **b)** Soziale Ungleichheit |  |
| Theorien zur sozialen Ungleichheit (Marxismus, Funktionalismus, Dahrendorf, Bourdieu u.a) | Entstehung der sozialen Ungleichheit, naturgegeben, Sozialdarwinismus, gottgewollt, gesellschaftlich bedingt, Privateigentum, Besitz von Produktionsmitteln, zwei Klassen (Bourgeoisie u. Proletariat), Rolle der Bewertung, soziale Normen, Humankapitaltheorie, Kampf um das Kapital, Marktprozess; Aristoteles, Scholastiker, Thomas von Aquin, Hinduismus, Rousseau, Karl Marx, Friederich Engels, Talcott Parsons, Ralph Dahrendorf, Gary Becker, Theodore W. Schultz, Bourdieu, Adam Smith |
| | Marxismus im Detail |
| | marxistische Gesellschaftstheorie, wissenschaftstheoretischer Idealismus, Hegel, Methode der Dialektik, Denken in Gegensätzen, Synthese des Sozialismus als Übergangstadium zum Kommunismus, Produktionsverhältnisse der Gesellschaft, Klassenkämpfe, ökonomische Interessenkonflikte, historischer Materialismus, Bewusstsein und Sozialverhältnisse, ökonomische Verhältnisse, dialektische Prozesse, sozialer Kampf, Kernaussagen des historischen Materialismus |
| | (Erwerbs-)Arbeit, Lohn | 
| | Produktivkräfte, Produktionsverhältnisse, Arbeitskraft, Produktionsmittel, Eigentumsverhältnisse, Herrschaftsverhältnisse, Arbeitsverhältnisse, Konsumationsverhältnisse, Produktionsweise |
| | Produktivkräfte, Produktionsverhältnisse, ideologischer Überbau, ökonomische Verhältniss, Überbau/Basis |
| | Karl Marx, Kapitalisten, fremde Arbeitskraft, Kapitalismus, Privatkapital, Merkmale der kapitalistischen Wirtschaftsweise, Privateigentum, Eigeninteresse, Gewinnorientierung, Reinvestition, Wettbewerb, Akkumulation, Mehrwert, Gebrauchswert, Tauschwert |
| |  |
| Die geschichtliche Entwicklung sozialer Ungleichheit |  |
| Ungleiche Lebensbedingungen |  |
| Soziale Mobilität und soziale Reproduktion |  |
| Soziale Randgruppen und Stigmatisierung | |
| | |
| **2. Sozialstruktur und soziale Schichtung** | |
| Die Sozialstruktur und der soziale Wandel (Praktische Anwendung) | |
| Sozialer Status und Prestige | |
| Soziale Stände, Klassen und Kasten | |
| | |
| Soziale Schicht und Schichtung |  |
| Neuere Schichtungsmodelle |  |
| Lebenslagen und soziale Lagen |  |
| Soziale Milieus und Lebensstile |  |
| **3. Migration** |  |


## Aufbau einer Analyse im Fach Soziologie

12 Punkte Fragen sind materialgebunden und setzen sich aus drei Teilen ([Lessing-Schule](https://lessing-schule.de/wp-content/uploads/2019/04/Leitfaden-Klausur-im-Fach-Sozialwissenschaften.pdf)).

1. Reproduktion
2. Analyse
3. Reflexion und Problemlösung

Die Anforderungen im Bereich der Darstellungsleistung sind:

Der SuS :

- strukturiert seinen Text schlüssig, stringent sowie gedanklich klar und bezieht sich dabei genau und konsequent auf die Aufgabenstellung. (**3 Punkte**)
- bezieht beschreibende, deutende und wertende Aussagen schlüssig aufeinander. (**2 Punkte**)
- belegt seine Aussagen durch angemessene und korrekte Nachweise (Zitate u.a.). (**2 Punkte**)
- formuliert unter Beachtung der Fachsprache präzise und begrifflich differenziert. (**3 Punkte**)
- schreibt sprachlich richtig (Grammatik, Orthographie, Zeichensetzung) sowie syntaktisch und stilistisch
sicher. (**2 Punkte**)

(Gesamt **20 Punkte**)


Der Kern einer Analyse ist in drei Schritten angelegt und kann aber je nach Fragestellung um zwei Punkte erweitert werden. Erst Theorie und Anwendung, dann Analyse weil gewöhnlich mehr Zeit vom Prüfer für die praktische Anwendung eingeplant wird. Am Ende ist oft Ihre ganz persönliche Sicht gefragt. Achten Sie jedoch darauf, dass Ihre Ansichten auch für Aussenstehende nachvollziehbar sind und sich damit nicht ausschliesslich auf Ihren eigenen Standpunkt beziehen, sondern mit allgemeingültigeren Zuschreibungen begründbar sind. Achten Sie ausserdem auf eine gewisse Stringenz in Ihrem Text und springen Sie nicht von einem EIndruck zu einem völlig anderen ohne überzuleiten. 

- Relevante Theorie mit Anwendung an den vorhandenen Text (WAS)
- Analyse (WIE)
- Interpretation (WOZU)
