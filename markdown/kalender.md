
# Kalender

## 2GSO

|Woche |Datum  |Themen                            | Konzepte | Aufgaben |
| :---: | --- | :---:                             | -------- | -------- | 
| T1a  |       | *Kap.1*                          |          |          |
| 1    |       | 1.1.1 - 1.1.2 <br> 1.1.3         | sozial, Wert- u. Normvorstellungen, Sozialnatur des Menschen, soziales Wesen, soziale Beziehung, kulturelles Wesen, <br> Entstehung der Soziologie, erfahrungswissenschaftliche Wissenschaften, Soziologie (Definition)          |  1-4 (Seite 48)        |
| 2    |       | 1.2.1 - 1.2.2 <br> 1.2.3 - 1.2.4 |          |          |
| 3    |       | 1.3.1 - 1.3.2 <br> 1.4.1         |          |          |
| 4    |       | 1.4.2 - 1.4.3 <br> 1.5.1 - 1.5.2 |          |          |
| 5    |       | 1.5.3 - 1.5.4                    |          |          |
| 6    |       | **DEC I,1**                      |          |          |
| T1b  |       | Kap.2   |          |  |
| T2a  |       | Kap.3   |          |  |
| T2b  |       | Kap.4   |          |  |
| T3a  |       | Kap.8   |          |  |
| T3b  |       | Artikel |          |  |

## 1GSO

|Woche |Datum |Themen   | Aufgaben | 
| ---  | ---  | ---     | -------- | 
| T1a  |      | - 9.1.1 - 9.1.2 <br> - 9.1.3 <br> - 9.1.4 <br> - 9.2.1 - 9.2.3 <br> - 9.2.4 - 9.2.5 <br> - 9.3.1 - 9.3.2  <br> - **DEC I,1** | - 1-2 (S.314) <br> - 3 (S.314)         | 
| T1b  |      | - 7.1 - 7.2.1 <br> - 7.2.1 - 7.2.2 <br> - 7.2.3 - 7.2.5 <br> - 7.2.3 - 7.2.5 <br> - 7.2.6 - 7.2.7 <br> - **DEC I,2**   |          | 
| T2a  |      | - 10.1.1 - 10.1.2 <br> - 10.2.1 - 10.2.2 <br> - 10.3.1 - 10.3.2 <br> - 10.3.3 - 10.2.4  <br> - **DEC II,1** |          | 
|      |      | - 10.1.1  |          - Sozialstruktur <br> - sozialer Wandel         | 
|      |      | - 10.2.1 - 10.2.2  | - sozialer Status <br> - soziales Prestige         | 
|      |      | - 10.3.1 - 10.3.2  | - der Stand <br> - Klassenmodelle         | 
|      |      | - 10.3.3 - 10.3.4  | - soziale Schicht und Schichtung <br>  - Schichtungsmodell        | 
|      |      | - DEC II,1  |                 | 
| T2b  |      | Immigration   |          | 

| T3a  |      | Examen   |          | 

## QCM

- regelmäßig wiederholen
- Multiple Choice Test
- jede Woche
- 20 Punkte



- 10.1.2 <br> - 10.2.1 - 10.2.2 <br> - 10.3.1 - 10.3.2 <br> - 10.3.3 - 10.2.4  <br> - **DEC II,1**