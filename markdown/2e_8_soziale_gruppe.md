# Die soziale Gruppe

## Übersicht

```mermaid
graph TD;
    A[soziale Gruppe] -->A1[Definition]
    A1 --> A11[Merkmale]
    A1 --> A12[Arten]
    A --> A2[Entstehung]
    A2 --> A21[Gruppenprozess]
    A2 --> A22[Verteilung von <br> Macht u. Ansehen]
    A --> A3[Beeinflussung <br> der Einzelnen]
    A3 --> A31[Bedeutung der Gruppe <br> für den Einzelnen]
    A3 --> A32[Gefahren des <br> Gruppeneinflusses]
```

## 1.1. Definition 

```mermaid
graph TD;
    A[Gruppe] -->A1[zusammengesetzt aus <br> Anzahl von Personen]
    A1 --> |über längeren Zeitraum| A11[Wechselbeziehung zwischen <br> Gruppenmitgliedern]
    A1 --> |im Gegensatz zu Menge| A12[bewusste Ansammlung]
    A11 --> |emotional| A2[Gruppenidentifikation]
    A12 --> |emotional| A2 
    A2 --> |durch Geborgenheit| A3[Wir-Gefühl]
    A3 --> |Zusammenhalt führt zu| A4[Gruppenkohäsion]
    A -->|soziale Struktur| A5[Gruppenstruktur]
    A5 --> |zeigt eine| A51[Normen- u. <br> Rollenstruktur]
    A5 --> |besitzt eine| A52[Macht-, Führungs- u. <br> Kommunikationsstruktur]
    A51 --> A6[soziales System]
    A52 --> A6
```

**Achtung**: 
- Rand*gruppe* ist keine *Gruppe* in diesem Kontext
- *Clique*: eine bestimmte Form der gruppe, Untergruppe die sich von der eigentlichen Gruppe ablöst

## 1.2. Entstehung

```mermaid
graph TD;
    A[3 Gründe der Gruppenbildung] -->A1[gemeinsam zu <br> erfüllende <br> Aufgabe]
    A --> A2[gemeinsame <br> Interessen]
    A --> A3[gegenseitige <br> Sympathie]
    A3 --> |abhängig von|A31[Kontakthäufigkeit]
    A3 --> |abhängig von|A32[wahrgenommene <br> Ähnlichkeit]
```


```mermaid
graph TD;
    A[4 typische Grundphasen der Gruppenbildung] --> A1[FORMING <br> Orientierungsphase]
    A1 --> A2[STORMING <br> Klärungsphase]
    A2 --> A3[NORMING <br> Vertrautheits- u. <br> Normierungsphase]
    A3 --> A4[PERFORMING]

```
## 1.3. Prozess der Gruppe

```mermaid
graph TD;
    A[Gruppe] --> |nicht statisch|B[im ständigen <br> Prozess der <br> Veränderung]
    B --> C[Gruppenprozess]
    C --> |Veränderung im <br> Gruppenleben|D[Gruppendynamik]
    D --> D1[Entstehung <br> sozialer <br> Normen] 
    D --> D2[Ausbildung <br> sozialer <br> Rollen]
    D1 --> E[2 gegenläufiuge <br> Prozesse]
    D2 --> E
    E --> E1[Druck der <br> Gruppe]
    E1 --> E11[soziale <br> Anerkennung]
    E1 --> E12[soziale <br> Rangordnung]
    E11 --> F[Sanktionen]
    E12 --> F
    F --> G[Angst aus Gruppe <br> ausgeschlossen <br> zu werden]
    E --> E2[Erwartungen <br> der Gruppe <br> von den <br> Mitgliedern]
    E2 --> H[Rollendifferenzierung]
    H --> I[positiv falls <br> es gruppenbezogen <br> ist]
```

## 1.4. Sozialer Rang

```mermaid
graph TD;
    A[Gruppe] -->|Hierarchie im <br> sozialen Gebilde <br> Beliebtheit <br> Fähigkeit <br> Kompetenz | A1[sozialer Rang]
    A1 --> A11[höhere Stellung]
    A1 --> A12[niedrige Stellung]
    A11 --> A111[Ansehen.<br> Macht <br> Einfluss]
    A12 --> A121[Indiv. wir <br> beeinflusst]
```

<br>

<br>

<br>

```mermaid
graph TD;
    A[Modell der rangdynamischen Position <br> Raoul Schindler ] --> A1[alpha-Position]
    A --> A2[beta-Position]
    A --> |unterscheidet folgende Positionen|A3[gamma-Position]
    A --> A4[omega-Position]
    A1 --> A11[Anführer]
    A2 --> A21[Experte]
    A3 --> A31[Mitglied]
    A4 --> A41[Gegenspieler]
    A41 --> A411[verhindert.<br> Verkrustung]
    A41 --> A412[destruktiver <br> Prozess]
```

## 2.1. Funktionen der Gruppe

```mermaid
graph TD;
    A[soziale Funktionen <br> der Gruppe] --> B[mitmenschliche Beziehungen]
    B --> |machen den <br> Menschen zum|B1[Menschen im <br> humanen Sinne]
    B1 --> |Aufbau der|B11[eigenen <br> Perönlichkeit]
    B --> |erlauben dem <br> Menschen|B2[das soziale <br> Verhalten <br> zu erlernen]
    B2 --> |führen zu einer|B21[handlungsfähigen <br> Gesellschaft]
    B --> |befriedigen|B3[soziale <br> Bedürfnisse]
    B --> |erlauben den|B4[eigenen Wert <br> zu erleben]
    B4 --> |entwickelt sich ein| B41[persönliches <br> Selbstwertgefühl]
    B --> |spielen eine <br> wichtige Rolle im| B5[Jugendalter]
    B5 --> B51[Peergroup]
    B51 --> |erlaubt| B511[Ablösung von der <br> Ursprungsfamilie]
    B51 --> |erleichtert| B5112[Suche nach <br> Identität]
    B5112 --> |gibt| B51121[Geborgenheit]
    B --> |erleichtert| B6[Verwirklichung <br> von Zielen]
    B6 --> |die eine| B61[gemeinsame <br> Anstrengung <br> erfordern]
    B61 --> |führt zu| B611[effektive <br> Gruppe]
    B611 --> B6111[großes Wissen]
    B611 --> B6112[Vielfalt <br> von <br> Können]
    B6111 --> |erlaubt|B62[originelle <br> Lösungen]    
    B --> |erlaubt den| B7[Vergleich der <br> Einzelnen in <br> der Gruppe]
    style B1 fill:#CCCCFF
    style B2 fill:#CCCCFF
    style B3 fill:#CCCCFF    
    style B4 fill:#CCCCFF    
    style B5 fill:#CCCCFF    
    style B6 fill:#CCCCFF    
    style B7 fill:#CCCCFF    
```

## 2.2. Das Konzept des sozialen Netzwerkes

```mermaid
graph TD;
    A[Konzept des sozialen Netzwerkes] --> A1[soziales <br> Netzwerk]
    A1 --- A11[Geflecht von <br> sozialen <br> Beziehungen]
    A1 --- A12[Gesamtheit <br> der Kontakte]
    A11 --- B1[Unterstützungs- <br> netzwerke]
    A12 --- B1 
    B1 --- |Bourdieu spricht von|C1[Hilfsquellen]
    C1 --> D1[Verbesserung <br> des Wohlbefindens]
    A --> A2[soziale <br> Unterstützung]
    A2 --- A21[Austausch von <br> Hilfsquellen]
    A2 --- A22[gegenseitige <br> Aufrechterhaltung]
    A2 --- A23[Verbesserung des <br> Wohlbefindens]
    A21 --- B2[Bewältigung von <br> kritischen <br> Lebensereignissen]
    A22 --- B2
    A23 --- B2 
    B2 --> C2[3 Wirkungen]
    C2 --> C21[Abschirmwirkung]
    C21 --- C211[Art Schutzschild]
    C2 --> C22[Pufferwirkung]
    C22 --- C221[negative <br> Stressreaktion <br> abschwächen]
    C2 --> C23[Toleranzwirkung]
    C23 --- C231[Ertragen von <br> belastenden <br> Situationen]
```

**Vorsicht**: soziale Netzwerke $\neq$ Social Network (Internet)


## 2.3. Gefahren einer Gruppe 

```mermaid
graph TD;
    A[Gefahren einer Gruppe] --> A1[Konformitätszwang]
    A1 --> A11[Gründe zur Unterwerfung]
    A11 --> A11a[mit der Meinung <br> richtig liegen]
    A11 --> A11b[Verantwortung nicht <br> allein tragen]
    A11 --> A11c[Annerkennung von <br> den anderen Mitgliedern]
    A11 --> A11d[nicht unangenehm <br> auffallen]
    A11 --> A11e[Angst ausgeschlossen <br> zu werden]
    A --> A2[Fehleinschätzung]
    A2 --> A21[groupthink]
    A --> A3[Entscheidungen mit <br> höherem Risikoniveau]
    A3 --> A31[risky shift]
    A --> A4[Gruppenpolarisierung]
    A --> A5[Deindividuation]
```

## 3.1 Primär- u. Sekundärgruppe
 
```mermaid
graph TD;
    A[Intensität der Wechselbeziehung] --> A1[Primärgruppe]
    A --> A2[Sekundärgruppe]
    A1 --> A1a[face to face <br> groups]
    A1 --> A1b[hohe <br> Gruppenkohäsion]
    A1 --> A1c[Intimgruppe]
    A1 --> A1d[starkes <br> Wir-Gefühl] 
    A2 --> A2a[geringe <br> Gruppenkohäsion]
```

## 3.2. Eigen- u. Fremdgruppe

```mermaid
graph TD;
    A[Zugehörigkeit] --> A1[Eigengruppe]
    A --> A2[Fremdgruppe]
```


## 3.3. Formelle u. informelle Gruppe 

```mermaid
graph TD;
    A[Grad der Organisation] --> A1[formelle Gruppe]
    A --> A2[informelle Gruppe]
```

![Simple Decision](https://gitlab.com/econmediadb/economics-course/-/raw/main/mindmap/018-Structured-Writing-Simple-Model-FR.png){width=70%}

![Alternative Decision](https://gitlab.com/econmediadb/economics-course/-/raw/main/mindmap/018-Structured-Writing-Model-Alternative-Decision-FR.png){width=70%}

![Institutions](https://gitlab.com/econmediadb/economics-course/-/raw/main/mindmap/018-Structured-Writing-Simple-Model-Institution-FR.png){width=70%}
