# Drei Kriterien zur sachlichen Beschreibung der Migrationsbewegungen 

## Raum 

```mermaid
graph LR;
    A[Raum] --> B[Welche Strecke wird zurückgelegt?]
    A --> C[Handelt es sich um eine Binnenwanderung?]
    A --> D[Welche Rolle spielen dir Netzwerke?]
```

## Dauer

```mermaid
graph LR;
    A[Dauer] --> B[saisonale Migration]
    A --> C[dauerhafte Migration]
    A --> D[Migration als Prozess]
```

## Motive

```mermaid
graph LR;
    A[Motive] --> B[gezwungene Migration]
    A --> C[freiwillige Migration]
```


