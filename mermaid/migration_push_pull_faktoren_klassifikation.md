# Push- und Pull-Faktoren

## Push-Faktoren

```mermaid
graph LR;
    A[Push-Faktoren] --> B[Wirtschaft]
    B --> B1[Arbeitslosigkeit/kaum Einkommen]
    B --> B2[teuere/nicht verfügbare Rohstoffe]
    B --> B3[hohe Lohnkosten/Steuern/Abgaben]
    A --> C[Soziale Probleme]
    C --> C1[Armut]
    C --> C2[religiöse Verfolgung]
    C --> C3[Unsicherheit]
    C --> C4[Diskriminierung]
    C --> C5[Ungerechtigkeit]
    C --> C6[Abgeschiedenheit]
    A --> D[Ereignisse]
    D --> D1[Katastrophen]
    A --> E[Demographische Probleme]
    E --> E1[Landknappheit]
    E --> E2[Überbevölkerung]
    A --> F[Politische Konflikte]
    F --> F1[politische Verfolgung]
    F --> F2[Krieg]
    F --> F3[Hohe Steuern]
    F --> F4[Enteignung]
    F --> F5[Geschlechter-/Rassendiskriminierung]
```

## Pull-Faktoren

```mermaid
graph LR;
    A[Pull-Faktoren] --> B[Wirtschaft]
    B --> B1[Hochkonjunktur]
    B --> B2[grosse Arbeitsnachfrage]
    B --> B3[gute Verdienstmöglichkeiten]
    B --> B4[gute Verkehrsanbindung]
    B --> B5[Wirtschaftsförderprogramme]
    A --> C[Soziale Vorteile]
    C --> C1[Sicherheit]
    C --> C2[Soziale Kontakte]
    C --> C3[gute Wohnmöglichkeiten]
    C --> C4[Toleranz]
    C --> C5[Bildungsmöglichkeiten]
    C --> C6[entwickeltes Gesundheitssystem]
    A --> D[Demographische Vorzüge]
    D --> D1[ausreichendes Flächenangebot]
    D --> D2[strukturierte Raumplanung]
    A --> E[Politische Vorteile]
    E --> E1[günstige Einwanderungsgesetze]
    E --> E2[Rechtssicherheit]
    E --> E3[Frieden]

 ```
