# Die drei Phasen des Migrationsprozesses (Shmuel Eisenstadt)

```mermaid
graph LR;
    A[Motivbildung] --> B[Migration]
    B --> C[Eingliederung in die <br> Aufnahmegesellschaft]
```

## 1. Motivbildung

```mermaid
graph TD;
    A0[Wahrnehmung belastender Umstände <br> Push-Faktoren] --> A
    A[Gefühl von Unsicherheit u. Unzufriedenheit] -->|führt zu| B[Migrationsüberlegung]
    B -->|Problemlösung ist die| C[Aufgabe der gewohnten Lebensumwelt]
    C -->|die Entscheidung ist <br> ein langer Prozess <br> wegen den |D[Risiken der Migration]
    D -->|Hoffnung auf|E[Berbesserung materieller u. <br> soziokultureller Lebensbedingungen]
    E -->|Informationen werden <br> gesammelt| F[Planung u. Vorbereitung]
```

## 2. Vorgang der Migration 

```mermaid
graph TD;
    A[Herkunftsort wird verlassen] -->|führt zu| B[radikale soziale Veränderung]
    B -->|benötigt eine|C1[Resozialisierung]
    B -->|erfordert eine Veränderung|C2[Selbstkonzept u. Werthierarchie]
    C1 --> D[neue Bedingungen <br> u. Anforderungen]
    C2 --> D

```

## 3. Eingliederung in die Aufnahmegesellschaft

```mermaid
graph TD;
    A[neue Denk- u. Umgangsformen erlernen] -->|erlaubt die <br> Integration <br> in die| B[Aufnahmegesellschaft]
    B --> C1[Institutionalisierung]
    C1 -->|Anpassung an die|C11[Werte <br> Normen <br> Gewohnheiten <br> Erwartungen <br> der Aufnahmegesellschaft]
    C11 --> C111[neue <br> Sprache]
    C11 --> C112[neue <br> Alltagstechniken]
    B -->|besteht aus 3 Teilprozessen|C2[Anpassung an die <br> Aufnahmegesellschaft]
    C2 -->|hängt stark ab von|C21[Bereitschaft der <br> Aufnahmegesellschaft <br> Opportunitäten <br> zu öffnen] 
    C2 -->|abhängig von| C22[Kompatibilität der <br> Werte beider Gruppen]
    B --> C3[Eindringen in die <br> institutionelle Sphären <br> Dispersion ] 
    C3 --> C31[Verschmelzung]
    C31 -->|Verlust von| C311[mitgebrachter <br> ethnischen <br> Gruppenidentität]
    C311 --> C3111[Absorption]
```

