# Chancen und Gefahren der Migration

```mermaid
graph TD;
    A[Migration] --> B[Chancen u. Gefahren]
    B --> B1[Ab- u. <br> Zuwanderung]
    B --> B2[über räumliche Ziele <br> und Verkehrswege]
    B --> B3[psychische <br> Belastung]
    B --> B4[physische <br> Belastung]
    B --> B5[finanzielle <br> Belastung]
```
