# Migrationskosten

```mermaid
graph TD;
    A[Migrationskosten]-->B[Reisekosten]
    B --> B1[Kosten für <br> Transport u. <br> Unterkunft]
    A --> C[Informations- <br> u. Suchkosten]
    C --> C1[Kosten bei <br> der Suche <br> nach Arbeit]
    A --> D[Opportunitäts- <br> kosten]
    D --> D1[Verdienstausfall <br> während der <br> Reise und <br> Arbeitssuche]
    A --> E[psychische <br> Kosten]
    E --> E1[Kosten bei der Überwindung <br> von Problemen die mit <br> dem Verlassen der vertrauten <br> Lebensumgebung verbunden sind]
```
